# Portfolio
___


## About me

My name is Łukasz Kowolik. I have been working in the bank for almost 8 years and for some time I have come to the conclusion that it is time for a change.


I decided that I would start my journey with manual testing.


My current goal is to develop my testing skills, continuously learn and share my learning experiences with the testing community.
___

## Tools I've used in my journey

- [Mantis Bug Tracker](https://www.mantisbt.org/) - fun and easy bug tracking tool<br>
- [Git](https://git-scm.com/) - a powerful version control system<br>
- [PyCharm](https://www.jetbrains.com/pycharm/) - one of the most popular IDE to code in Python but i used it mostly to learn version control through IDE.<br>
- [PostgreSQL](https://www.postgresql.org/) - an object-relational database system<br>
- [Gitlab](https://about.gitlab.com/) - online code repository<br>
- [Laragon](https://laragon.org/) - easy to use and isolated environment that includes PHP, MySQL/MariaDB and Apache preinstalled (I used this to create required environment for MantisBT on my PC)
- [JIRA](https://www.atlassian.com/pl/software/jira) - probably the most popular project management tool 
___

## Courses

- [udemy.com](https://www.udemy.com/course/kurs-testowania-oprogramowania/) - Podstawy manualnego testowania oprogramowania (ENG: Fundamentals of manual software testing)<br>
- [strefakursow.pl](https://strefakursow.pl/kursy/programowanie/kurs_sql_-_bazy_danych_od_podstaw.html) - Kurs SQL - bazy danych od podstaw ;PostgreSQL (ENG: SQL Course - basics of databases ;PostgreSQL)<br>
- [strefakursow.pl](https://strefakursow.pl/kursy/programowanie/kurs_oracle_sql_-_bazy_danych_od_podstaw.html) - Kurs Oracle SQL - bazy danych od podstaw (ENG: Oracle SQL Course - basics of databases)  - in progress<br>
- [jaktestowac.pl](https://jaktestowac.pl/git/) - Git dla testerów (ENG: Git for Testers)<br>
- [jaktestowac.pl](https://jaktestowac.pl/course/playwright-wprowadzenie/) - Playwright wprowadzenie (ENG: Playwright introduction) - in progress <br>
___

## Certificates

- [ISTQB](https://www.gasq.org/en/certification/check-a-certificate.html) - ISTQB Certified Tester Foundation Level - id: 101180<br>
___

## Books

- [Zawód tester](https://lubimyczytac.pl/ksiazka/291227/zawod-tester) - R. Smilgin <br>
- [Certyfikowany tester ISTQB. Poziom podstawowy](https://lubimyczytac.pl/ksiazka/4943677/certyfikowany-tester-istqb-poziom-podstawowy) - A. Roman, L. Stapp<br>
---

## Blogs read
- [jaktestowac.pl](https://jaktestowac.pl)<br>
- [remigiuszbednarczyk.pl](https://remigiuszbednarczyk.pl/)<br>
- [szkoleniedlaqa.pl](https://szkoleniedlaqa.pl/)<br>
___

## My sample test scenarios
- [Test scenarios for Winamp v5.8 Build 3660](https://docs.google.com/spreadsheets/d/1zlyVuFuusGvRy5MkUMbCmI-Pq3uL9f0d/edit?usp=sharing&ouid=103180022136401024562&rtpof=true&sd=true) 

___

## My sample test summary reports
- [Test summary report for a certain calculator app](https://drive.google.com/file/d/1Fzt9XxrsnLcDmDGTB2lX4ZFHukUDS187/view?usp=sharing)

___

## My personal projects
- Defect reports for [Mr. Buggy](http://mrbuggy.pl/) apps using JIRA software (polish language) - [see current progress](https://drive.google.com/drive/folders/19QY1lAcrob0js5iRTOL7LkMEVBMFbJIy?usp=sharing)
- [Utest Academy](https://www.utest.com/) - done! all practical test courses passed
